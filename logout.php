<?php

    //Avataan tietokantayhteys
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
    //
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    if ($tietokanta != null) {
        try {
            $_SESSION['login'] = false;
            unset($_SESSION['kayttaja_id']);
            header('Location: index.php');
        } catch (PDOException $pdoex) {
            print "Käyttäjän tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
    

?>
