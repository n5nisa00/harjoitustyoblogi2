<?php include_once 'inc/top.php';?>

    <div class="container">

      <div class="starter-template">
          
          <?php
          $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
          //$kayttaja_id=1;
          
          // Avataan tietokantayhteys.
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          
          if ($_SERVER['REQUEST_METHOD']==='POST') {
            try {
                $kayttaja_id=$_SESSION['kayttaja_id'];
                // Luetaan tiedot lomakkeelta.
                $id = filter_input(INPUT_POST,'kirjoitus_id',FILTER_SANITIZE_NUMBER_INT);
                $kommentti = filter_input(INPUT_POST, 'kommentti',FILTER_SANITIZE_STRING);
                $kysely = $tietokanta->prepare("INSERT INTO kommentti(kirjoitus_id,kayttaja_id,teksti) VALUES (:id,:kayttaja_id,:kommentti)");
                
                $kysely->bindValue(':kommentti',$kommentti,PDO::PARAM_STR);
                $kysely->bindValue(':id',$id,PDO::PARAM_INT);
                $kysely->bindValue(':kayttaja_id',$kayttaja_id,PDO::PARAM_INT);

                $kysely->execute();
                header("Location: blogi.php?id=$id");
                exit;
            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
                print "kirjoitus id: " . $id . " Kommentti: " . $kommentti . " Käyttäjä ID: " . $kayttaja_id;
            }
        }
          
        try {
            // Muodostetaan suoritettava sql-lause.
            //$sql = 'SELECT * FROM kirjoitus WHERE id=' . $id . ' LIMIT 1';
            $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id WHERE kirjoitus.id=" . $id . " LIMIT 1";
            
            $stmt = $tietokanta->prepare($sql);
            $stmt->execute();
           
            // Suoritetaan kysely tietokantaan.
            $tietue = $stmt->fetch();
            
            if ($tietue) {
                print '<div class="kirjoitus">';
                    print '<h1>' . $tietue['otsikko'] . '</h1>';
                    print '<h4>' . date("d.m.y H.i", strtotime($tietue['paivays'])) . '&nbspby&nbsp' . $tietue['tunnus'] . '</h4>';
                    print '<p>' . $tietue['teksti'] . '</p>';
                    //print '<div class="form-group"><label for="teksti">Kommentit</label><textarea class="form-control" id="teksti" name="teksti" rows="3"></textarea></div>';
                    print '<label for="teksti">Kommentit</label>';
                print '</div>';
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
    if (isset($_SESSION['kayttaja_id'])) {
        ?>
          <form id="lisaa_kommentti" method="post" action="<?php print($_SERVER['PHP_SELF']);?>">
              <input type="hidden" name="kirjoitus_id" value="<?php print $tietue['id'];//$tietue->id?>">
              <textarea name="kommentti" id="kommentti" rows="3" cols="50"></textarea><br>
              <button type="submit" class="btn btn-primary">Lisää kommentti</button>
          </form>
            <?php
            
    }
        try {
                       
            // Muodostetaan suoritettava sql-lause.
            //$sql = 'SELECT * FROM kirjoitus ORDER BY id DESC';
            $sql = "SELECT *,kommentti.id as id FROM kommentti INNER JOIN kayttaja ON kommentti.kayttaja_id = kayttaja.id WHERE kirjoitus_id=" . $id . " ORDER BY paivays desc";
           
            // Suoritetaan kysely tietokantaan.
            $kysely = $tietokanta->query($sql);
            
            if ($kysely) {
                print '<div class="kommentit">';
                print '<ul>';
                while ($tietue = $kysely->fetch()) {  
                    print '<li>' . $tietue['teksti'] . '&nbsp<i>' . date("d.m.y H.i", strtotime($tietue['paivays'])) . '&nbspby&nbsp' . $tietue['tunnus'] . '</i>&nbsp&nbsp';
                if (isset($_SESSION['kayttaja_id'])) {
                    if ($_SESSION['kayttaja_id'] == $tietue['kayttaja_id']) {
                        print '<a href="poista_kommentti.php?id=' . $id . '&kirjoitus_id=' . $tietue['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></li>';
                    }
                    
                }
                    
                    //print '<hr>';
                    //
                }
                print '</ul>';
                print '</div>';
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
        ?>
      </div>

    </div><!-- /.container -->
    
<?php include_once 'inc/bottom.php';?>