<?php
include_once 'inc/top.php';

$viesti = "";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    //Avataan tietokantayhteys
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
    //
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    if ($tietokanta != null) {
        try {
            $tunnus = filter_input(INPUT_POST,'tunnus', FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST, 'salasana', FILTER_SANITIZE_STRING));
            
            $sql = "SELECT * FROM kayttaja where tunnus='$tunnus' AND salasana='$salasana'";
            
            $kysely = $tietokanta->query($sql);
            
            if ($kysely->rowCount() === 1) {
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
            }
        } catch (PDOException $pdoex) {
            print "Käyttäjän tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
}
?>
    <div class="container">
        <div class="starter-template">
            <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
                <div class="form-group">
                    <label for="text">Tunnus:</label>
                    <input type="text" class="form-control" id="text" name="tunnus">
                </div>
                <div class="form-group">
                    <label for="pwd">Salasana:</label>
                    <input type="password" class="form-control" id="pwd" name="salasana">
                </div>
            <button type="submit" class="btn btn-default">Kirjaudu</button>
            </form>
            <div>
                <p><?php echo $viesti?></p>
            </div>
        </div>
    </div><!-- /.container -->

<?php include_once 'inc/bottom.php';?>
